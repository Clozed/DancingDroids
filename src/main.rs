/*

    PROJET DANCING DROIDS
    MARCORY Claire - 19006820
    TOULLIOU Thomas - 19004228
    ANTHONIPILLAI Jackson - 19002768

*/

use rand::Rng;
use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;

/// enum pour les quatres orientation (nord, est, sud, ouest)
#[derive(Debug, Clone, PartialEq)]
enum Orientation {
    North,
    East,
    South,
    West,
}

/// enum pour les trois instructions (tourner de 90° à droite, à gauche, et avancer d'une case selon l'orientation du robot)
#[derive(Debug, Clone)]
enum Instruction {
    L,
    R,
    F,
}

/// structure qui gère les robots avec un id, une position x et y, une orientation,
/// et un vecteur qui contient toutes les instructions à effectuer pour se robot
#[derive(Debug, Clone)]
struct Robot {
    id: usize,
    x: i32,
    y: i32,
    orientation: Orientation,
    instructions: Vec<Instruction>,
}

/// fonction qui va permettre d'afficher les informations du terrain et des robots
fn print_info(robots: &Vec<Robot>, x_max: i32, y_max: i32) {
    println!("Terrain {{ x_max = {}; y_max = {} }}", x_max, y_max);
    println!("Robots [");
    let iter = robots.iter();
    // un itération dans notre vecteur de robots qui permettra d'afficher toutes les informations dont on a besoin
    for robot in iter {
        println!(
            "  {{ id = {}, x = {}; y = {}; orientation: {:?}, instructions: {:?} }}",
            robot.id, robot.x, robot.y, robot.orientation, robot.instructions
        );
    }
    println!("]");
}

/// fonction qui permet d'afficher le monde ainsi que les robots à leurs positions
/// il peut y avoir des problèmes dans l'affichage du plateau (ex: repère en haut mal placé si les chiffres sont trop grands)
fn print_world(pos: &Vec<Robot>, x0: usize, y0: usize) {
    let mut x1 = 0;
    let mut y1 = 0;
    let mut _tableau: Vec<i32> = Vec::new();
    let mut _tableau = vec![vec![' '; x0]; y0];

    for robot in pos.iter() {
        let rx = robot.x as usize;
        let ry = robot.y as usize;
        match robot.orientation {
            Orientation::North => _tableau[rx][ry] = '↑',
            Orientation::East => _tableau[rx][ry] = '→',
            Orientation::South => _tableau[rx][ry] = '↓',
            Orientation::West => _tableau[rx][ry] = '←',
        }
    }

    print!("    ");
    while y1 < y0 {
        if y1 >= 1000 {
            print!("{}", y1);
            y1 = y1 + 1;
        }
        if y1 >= 100 {
            print!("{} ", y1);
            y1 = y1 + 1;
        }
        if y1 >= 10 {
            print!("{}  ", y1);
            y1 = y1 + 1;
        } else {
            print!("  {} ", y1);
            y1 = y1 + 1;
        }
    }
    println!("");

    for z in 0..x0 {
        print!("{}", x1);
        if x1 >= 1000 {
            print!("");
        }
        if x1 >= 100 {
            print!(" ");
        }
        if x1 >= 10 {
            print!("  ");
        } else {
            print!("   ");
        }
        x1 = x1 + 1;
        for w in 0..y0 {
            print!(". {} ", _tableau[w][z]);
        }
        println!(" ");
    }
}

/// fonction qui permet le tirage d'une instruction aléatoire
fn random(min: usize, max: usize) -> usize {
    let mut tirage = rand::thread_rng();
    tirage.gen_range(min, max + 1)
}

/// fonction qui permet de vérifier, avant le mouvement du robot, si ce dernier ne sera pas en dehors
/// des limites du monde en 2d crééer de dimension (x_max, y_max)
fn in_bound(x_max: i32, y_max: i32, x: i32, y: i32) -> bool {
    if x <= x_max && y <= y_max && x >= 0 && y >= 0 {
        return true;
    }
    println!("The robot will fall !");
    return false;
}

/// fonction qui permet de chercher dans une HashMap contenant toutes les positions des robots sauf celui effectuant ses instructions
/// si ce dernier et un autre robot immobile auront des coordonnées identiques, càd,
/// si le robot en mouvement effectuera une collision
fn collision(robots: Vec<Robot>, x: i32, y: i32) -> bool {
    let mut pos = HashMap::new();
    for (id, robot) in robots.iter().enumerate() {
        pos.insert((robot.x, robot.y), id);
    }
    let here = (x, y);
    let collision = pos.get(&here);
    match collision {
        Some(_id) => true,
        None => false,
    }
}

/// fonction qui gère l'instruction "L", où le robot doit tourner de 90° vers la gauche
fn instr_left(ori: Orientation) -> Orientation {
    // Renvoie la nouvelle orientation du robot
    match ori {
        Orientation::North => Orientation::West, // Du nord à l'ouest
        Orientation::East => Orientation::North, // De l'est au nord
        Orientation::South => Orientation::East, // Du sud à l'est
        Orientation::West => Orientation::South, // De l'ouest au sud
    }
}

/// fonction qui gère l'instruction "R", où le robot doit tourner de 90° vers la droite
fn instr_right(ori: Orientation) -> Orientation {
    // Renvoie la nouvelle orientation du robot
    match ori {
        Orientation::North => Orientation::East, // Du nord à l'est
        Orientation::East => Orientation::South, // De l'est au sud
        Orientation::South => Orientation::West, // Du sud à l'ouest
        Orientation::West => Orientation::North, // De l'ouest au nord
    }
}

/// fonction qui gère l'instruction "F", où le robot doit avancer d'une case selon son orientation actuelle, on a besoin ici :
/// - des limites du monde pour vérifier si le robot tombera ou non à la suite de son avancement (s'il tombe, l'instruction sera ignorée),
/// - du robot qui est train d'effectuer ses instructions
/// - d'un vecteur de robots qui contient tous les robots sauf celui qui est en train de bouger afin de vérifier s'il y aura une collision après le mouvement
///   (si il y a une collision, l'instruction sera ignorée, et un message qui informe de la position de la collision devra être envoyer)
fn instr_forward(x_max: i32, y_max: i32, mut robot: Robot, pos: Vec<Robot>) -> Robot {
    // Renvoie le robot à sa nouvelle position après avancement d'une case
    let delta = match robot.orientation {
        Orientation::North => (0, 1),
        Orientation::East => (1, 0),
        Orientation::South => (0, -1),
        Orientation::West => (-1, 0),
    };
    let (delta_x, delta_y) = delta;
    let collided = collision(pos, robot.x + delta_x, robot.y + delta_y);
    let in_world = in_bound(x_max, y_max, robot.x + delta_x, robot.y + delta_y);
    if in_world {
        if collided {
            println!(
                "Robot ID<{}>, collision in ({}, {})",
                robot.id,
                robot.x + delta_x,
                robot.y + delta_y
            );
        } else {
            robot.y += delta_y;
            robot.x += delta_x;
        }
    }
    return robot;
}

/// fonction qui parcours le vecteur d'instructions du robot pour effectuer ces dernières
/// renvoie à la fin de son execution un nouveau vecteur de robot qui contiendra tous les robots
/// ainsi que celui qui vient d'effectuer ses instructions, à sa position finale
fn move_robot(x_max: i32, y_max: i32, robot: Robot, mut positions: Vec<Robot>) -> Vec<Robot> {
    let todo = positions[0].instructions.clone();
    positions.remove(0); // retirer le robot qui est train d'effectuer ses instructions du vecteur de robots pour éviter un cas où il retournera à sa position de départ et engendre une collision avec lui-même? son image du passé?
    let mut droid = robot.clone();
    let iter = todo.into_iter();
    for i in iter {
        match i {
            Instruction::L => droid.orientation = instr_left(droid.orientation), // tourner vers la gauche
            Instruction::R => droid.orientation = instr_right(droid.orientation), // tourner vers la droite
            Instruction::F => {
                // avancer d'une case dans la direction du robot
                let pos: Vec<Robot> = positions.clone();
                droid = instr_forward(x_max, y_max, droid, pos);
            }
        }
    }
    positions.push(droid); // ajouter au vecteur le robot qui est maintenant à sa position finale après avoir effectuer toutes ses instructions
    return positions;
}

fn main() {
    // Ouvrir le fichier qui contient toutes les informations nécessaire, organisées de préférence !!
    let mut file = File::open("../two_robots.txt").expect("Can't open this file!");

    // Créer un unique String qui contient ce qui est écrit dans le fichier ci-dessus
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Can't read the file...");

    // Créer un vecteur qui va permettre de séparer un unique String en plusieurs String pour réutiliser les différents informations plus facilement
    let mut data: Vec<String> = Vec::new();
    for token in contents.split_whitespace() {
        data.push(String::from(token));
    }

    let x_max: i32 = data[0].parse().unwrap(); // Dimension x du monde
    let y_max: i32 = data[1].parse().unwrap(); // Dimension y du monde
                                               // retirer ses informations maintenant inutiles du vecteur de données
    data.remove(0);
    data.remove(0);
    
    // Créer un vecteur qui contiendra tous les robots dont on aura récupérer les informations du String data
    let mut droids: Vec<Robot> = Vec::new();
    
    let mut i = 0; // variable utilisée pour gérer les indices du vecteur data
    let mut j = 0; // variable utilisée pour arrêter la boucle
    
    let mut nb_robots = 0;
    for token in &data {
        if token == "W" || token == "E" || token == "S" || token == "N" {
            // compter le nombre de robots à partir de la seule information qui leur est unique et dont on connait toutes les possibilitées, càd l'orientation de départ
            // (car les coordonnées x et y dépendent de la taille du monde (donc beaucoup de possibilités) et il est possible qu'il n'y ai pas d'instruction avec la version 0.3.0)
            nb_robots += 1;
        }
    }

    data.push("".to_string()); // pour la version 0.3.0, car si le dernier robot du fichier texte n'a pas d'instructions, il y aura un index out of bound
    
    loop {
        // boucle qui va permettre d'initialiser chaques robots et de les stocker dans le vecteur droids
        let id = j; // id du robot
        let x = data[i].parse().unwrap(); // position de départ x du robot
        let y = data[i + 1].parse().unwrap(); // position de départ y du robot

        let orientation = data[i + 2].as_str(); // orientation de départ du robot
        let orientation = match orientation {
            "N" => Orientation::North,
            "E" => Orientation::East,
            "S" => Orientation::South,
            "W" => Orientation::West,
            _ => panic!("Where do you want to go...?"),
        };

        let mut instructions: Vec<&str> = Vec::new();
        if data[i + 3].len() <= 1 {
            // vérifier la taille des instructions (qui est normalement le seul string dont la taille est => 1)
            if data[i + 3] != "F" && data[i + 3] != "L" && data[i + 3] != "R" {
                // vérifier le cas où il y a une unique insturctions à effectuer
                let vec1: Vec<&str> = vec!["F", "L", "R"];
                for _i in 0..random(1, 10) {
                    // prendre un nombre d'instructions aléatoires entre 1 et 10
                    let add = vec1[random(0, 2)]; // prendre un élément aléatoirement dans vec1 qui contient les trois possibilités d'instructions exécutables
                    instructions.push(add); // ajouter l'instruction tirée aléatoirement au vecteur d'instructions
                }
                i += 3;
            }
        } else {
            instructions = data[i + 3].split("").collect(); // séparer les instructions à chaque caractères et les placer dans un vecteur
                                                            // retirer les premiers et derniers éléments du vecteur car ils sont vides (au println j'obtient un : "")
            &instructions.remove(0);
            &instructions.remove(instructions.len() - 1);
            i += 4;
        }

        let mut todo: Vec<Instruction> = Vec::new(); // vecteur d'instruction du robot
        for token in instructions {
            match token {
                "L" => todo.push(Instruction::L), // Si "L", ajouter l'Instruction L dans le vecteur d'instruction todo du robot
                "R" => todo.push(Instruction::R), // Si "R", ajouter l'Instruction R dans le vecteur d'instruction todo du robot
                "F" => todo.push(Instruction::F), // Si "F", ajouter l'Instruction F dans le vecteur d'instruction todo du robot
                _ => panic!("ERROR ! Unknown Instruction !!"), // Aucun des cas précédents, instruction inconnue
            };
        }

        let robot = Robot {
            id: id,
            x: x,
            y: y,
            orientation: orientation,
            instructions: todo,
        }; // Créer le nouveau robot
        droids.push(robot); // Ajouter le robot au vecteur qui contient tous les robots

        j += 1;
        if j == nb_robots {
            // s'arrêter lorsque j == nb_robots
            break;
        }
    }

    // afficher les informations initiales de tous les robots
    println!("=== Initial state ===\n");
    print_info(&droids, x_max, y_max);

    print_world(&droids, (x_max + 1) as usize, (y_max + 1) as usize);

    let mut positions = droids.clone();
    let mut i = 0;
    loop {
        let robot: Robot = positions[0].clone(); // le robot qui va effectuer ses instructions est le premier du vecteur de robot
        positions = move_robot(x_max, y_max, robot, positions); // La position initiale du robot a été supprimer et sa position finale a été ajouter dans le vecteur pendant l'execution de la fonction
                                                                // on a maintenant un nouveau vecteur dont l'indice 0 est le robot suivant qui doit effectuer ses instructions,
                                                                // c'est reparti pour un tour, jusqu'à ce que tous les robots aient effectué leurs instructions !

        i += 1;
        if i == nb_robots {
            // arrêter la boucle lorsque i == nb_robots, càd lorsque tous les robots auront effectuer leurs instructions
            break;
        }
    }

    // afficher les informations finales de tous les robots
    println!("\n=== Final state ===\n");
    print_info(&positions, x_max, y_max);

    print_world(
        &positions,
        (x_max + 1) as usize,
        (y_max + 1) as usize,
    );
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_inbound() {
        let x_max = 5;
        let y_max = 5;
        // robot in map
        assert!(in_bound(x_max, y_max, 0, 0));
        assert!(in_bound(x_max, y_max, 2, 3));
        // Robot outside of the map.
        assert_eq!(in_bound(x_max, y_max, 6, 6), false, "outside positive");
        assert_eq!(in_bound(x_max, y_max, 2, -1), false, "y outside neg");
        assert_eq!(in_bound(x_max, y_max, -1, 2), false, "x outside neg");
        assert_eq!(in_bound(x_max, y_max, 2, 6), false, "y outside positive");
        assert_eq!(in_bound(x_max, y_max, 6, 2), false, "x outside positive");
        assert_eq!(in_bound(x_max, y_max, -1, -1), false, "totally outside neg");
    }
}
